#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define size  100000000
#define init_time clock_t t
#define start_time t = clock()
#define get_time (1000*((long)clock() -t))/CLOCKS_PER_SEC

int main(){
	char * word = (char *) malloc(sizeof(char) * size);
	if (word == NULL)
	{
		printf("Bad memory allocation\n");
		exit(1);
	}
	for (long i=0; i<size-1; i++) word[i] = 'a' + (rand() % 27);
	printf("Word generated of size %d Mo\n",  size/1000000);
	word[size/2] = '@'; //finding the '@' will provides the raw speed of simple regexp matching
	regex_t regex;
	regmatch_t matches[1];
	int error;
	error = regcomp(&regex, "@", REG_EXTENDED);
	if (error){
		fprintf(stderr, "Could Not compile regexp\n");
		exit(1);
	}
	init_time;
	start_time;
	error = regexec(&regex, word, 1, matches, 0);
	if (error == 0){
		printf("regex:match:%d (ms):%c:%d\n", get_time, word[matches[0].rm_so], matches[0].rm_so);
	}
	else
	{
		printf("regex:no match:%d::(ms)\n", get_time);
	}
	start_time;
	char * pos =  (char *) memchr(word, '@', size);
	if (pos != NULL)
	{
		printf("memchr:match:%d (ms):%c:%d\n", get_time, *pos, pos - word);
	}
	else
	{
		printf("memchr:match:%d:(ms)\n", get_time);
	}
	free(word);
	return 0;
}
