from . import semigroups
from . import monoids
FiniteMonoid = monoids.FiniteMonoid
FiniteSemigroup = semigroups.FiniteSemigroup
from . import stamps
Stamp = stamps.Stamp
from . import transitions
TransitionSemigroup = transitions.TransitionSemigroup
TransitionMonoid = transitions.TransitionMonoid
TransitionStamp = transitions.TransitionStamp
