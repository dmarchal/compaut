"""
Define classical finite semigroups and monoids.
"""
import compAut.algebra as algebra

__all__ = ["U1", "U2", "aperiodic_brandt_semigroup(n)"]

def U1():
	"""
	Monoids of size 2, {0, 1} with integer product and 1 as identity.
	"""
	return algebra.FiniteMonoid([0, 1], product=lambda x,y:x*y, identity=1)

def U2():
	"""
	Left trivial monoid with 0 as identity and 1.2 = 2 and 2.1 = 1.
	"""
	def f(x, y):
		if all((x, y)):
			return y
		else:
			return x or y

	return algebra.FiniteMonoid([0, 1, 2], product=f, identity=0)

def aperiodic_brandt_semigroup(n):
	"""
	Return the aperiodic Brandt semigroup of size n. The 0 is represented by 0.
	See: https://en.wikipedia.org/wiki/Brandt_semigroup

	"""
	elements = [(i, j) for i in range(n) for j in range(n)]
	elements.append(0)
	def f(x, y):
		if all((x, y)) and x[1] == y[0]:
			return (x[0], y[1])
		return 0
	return algebra.FiniteSemigroup(elements, product=f)

