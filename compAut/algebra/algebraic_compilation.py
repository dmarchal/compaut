"""
We take a stamp a build an evaluator for it.
We return a word representing it.
"""

def sg_compile(stamp):
	M = stamp.monoid
	rep = {i:x for i, x in enumerate(M)}
	rev_rep = {x:i for i,x in rep.items()}
	mult_table = {i:{j: rev_rep[M(rep(i), rep(j))] for j in rep } for i in rep}
	start = stamp(())
	generators

