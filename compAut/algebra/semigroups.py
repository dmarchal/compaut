import collections.abc
import itertools
import types
from compAut.metas import allExcept, allIn
import compAut
from collections import defaultdict
import networkx as nx
from .eggbox import eggbox

class GeneratorIter:
	def __init__(self, generator, contains=None):
		self.generator = generator
		self.cache = set()
		if contains is None:
			def contains(element):
				if element not in self.cache:
					return element in self.generator()
				return True
		self.__contains = contains

	def __iter__(self):
		return self.generator()

	def __contains__(self, el):
		return self.__contains(el)

class Universe(collections.abc.Iterable, collections.abc.Container):
	"""
	Class to manipulate carrier set of semigroups.
	It support set operations (cartesian product, union, quotient, subset)
	and __contains__.
	Expect a generator or an iterable to be build.
	"""
	def __init__(self, elements, contains=None):
		if type(elements) is types.FunctionType:
			elements = GeneratorIter(elements, contains=contains)
		self.elements = elements
		if contains is None:
			contains = self.elements.__contains__
		self.__contains = contains

	def __iter__(self):
		return iter(self.elements)

	def __contains__(self, element):
		return self.__contains(element)

	def union(self, other, *args):
		def gen():
			yield from iter(self)
			yield from filter(lambda e: e not in self, other)

			def contains(element):
				return element in self or element in other
		return self.__class__(GeneratorIter(gen, contains), *args)

	def cartesian_product(self, other, *args):
		def gen():
			for x in self:
				for y in other:
					yield (x, y)
		def contains(element):
			return element[0] in self and  element[1] in other
		return self.__class__(GeneratorIter(gen, contains), *args)

	def sub(self, filter_fct, *args):
		def gen():
			yield from filter(filter_fct, self)
		def contains(element):
			return filter_fct(element) and element in self
		return self.__class__(GeneratorIter(gen, contains), *args)

	def quotient(self, quotient_map, *args):
		"""
		A quotient_map is mapping (dictlike) that maps
		elements to a representent (which should be in self).

		In particular, the element in the quotient are exactly elements such that
		quotient_map(x) == x.
		"""
		def gen():
			seen = set()
			for x in self:
				y = quotient_map[x]
				if y not in seen:
					seen.add(y)
					yield y
		def contains(self, element):
			return  element == quotient_map(element) and element in self
		return self.__class__(GeneratorIter(gen, contains), *args)

	def add_element(self, element):
		def gen():
			yield element
			yield from self
		def contains(el):
			return el == element or el in self
		return self.__class__(GeneratorIter(gen, contains), *args)

class FiniteUniverse(Universe):
	def __len__(self):
		return len(self.elements)


class Semigroup(Universe):
	"""
		A generic class for semigroups.
		Semigroups is not assume to be finite, all operations are done
		lazily and nothing is check. If product is not associative,
		it will output garbage silently.
	"""
	def __init__(self, elements, product=None, contains=None, cayley_graph=None, left_cayley_graph=None, gen_cayley=None, cached_product=True):
		"""
		+ product should be an associative binary functions over elements
			if product is not provided, callable on elements will be tried.
			if its not is not provided, __add__ on elements will be used.
		+ elements should admits __contains__ and __iter__ methods.
		+ contains will overload the contains of element in case it is a iterator.
		+ cayley_graph will be use to accelerate computation. Discover on the way if not given.
		"""
		super().__init__(elements, contains=contains)
		if product is None and "__call__" in dir(elements):
			product = elements
		product = (lambda x, y: x+y) if product is None else product
		if cayley_graph is None and "cayley_graph" in dir(elements):
			cayley_graph = cayley_graph
		if left_cayley_graph is None and "left_cayley_graph" in dir(elements):
			left_cayley_graph = left_cayley_graph

		if cached_product:
			product_table = {} # dictionnary of pairs (x, y) => product(x, y)
			def _product(x, y):
				if (x, y) not in product_table:
					product_table[(x, y)] = product(x, y)
				return product_table[(x, y)]
		else:
			_product = product
		self.product = _product
		if cayley_graph is not None:
			self._cayley_graph = cayley_graph
		if left_cayley_graph is not None:
			self._left_cayley_graph = left_cayley_graph
		self.gen_cayley = gen_cayley if gen_cayley is not None else self.elements
		self._fresh = object()

	def _read(self, e, f, direction = "R"):
		if direction == "R":
			graph = self.cayley_graph
		elif direction == "L":
			graph = self.left_cayley_graph
		else:
			raise ValueError ("Undefined direction")
		if f not in self.gen_cayley:
			raise ValueError("Not a generator")
		for g, gen in graph[e].items():
			if gen["gen"] == f:
				return g
		g = self.product(e, f)
		graph.add_edge(e, g, gen=f)
		return g

	def read_cayley(self, e, f):
		self._read(self, e, f)

	def read_left_cayley(self, e, f):
		self._read(self, e, f, direction="L")

	@property
	def cayley_graph(self):
		if not hasattr(self, "_cayley_graph"):
			self.generates_cayley_graphs()
		return self._cayley_graph

	@property
	def left_cayley_graph(self):
		if not hasattr(self, "_left_cayley_graph"):
			self.generates_cayley_graphs()
		return self._left_cayley_graph

	def generates_cayley_graphs(self):
		self._left_cayley_graph = G = nx.DiGraph()
		self._cayley_graph = LG = nx.DiGraph()
		G.add_edges_from([(x, self(x, y), {"gen":y}) for x in self for y in self.gen_cayley])
		LG.add_edges_from([(x, self(y, x), {"gen": y}) for x in self for y in self.gen_cayley])

	@classmethod
	def generated_by(cls, elements, product=None, **kwargs):
		"""
		Compute the semigroup generated by "elements" applying them "product".
		Construct the cayley Graph on the way.
		If product is not given, the __add__ operator on elements will be used.
		"""
		product = (lambda x, y: x+y) if product is None else product
		if "__iter__" not in dir(elements):
			elements = (elements, )
		cayley_graph = nx.DiGraph()
		left_cayley_graph = nx.DiGraph()
		def gen():
			last = elts = tuple(elements)
			yield from last
			seen = {x:x for x in last}
			while last:
				next_el = []
				for e in last:
					for f in elts:
						g =  product(e, f)
						if g not in seen:
							seen[g] = g
							next_el.append(g)
							yield g
						else:
							g = seen[g]
						cayley_graph.add_edge(e, g, gen=f)
						g =  product(f, e)
						if g not in seen:
							seen[g] = g
							next_el.append(g)
							yield g
						else:
							g = seen[g]
						left_cayley_graph.add_edge(e, g, gen=f)
				last = next_el
		return cls(gen, product, cayley_graph=cayley_graph, left_cayley_graph=left_cayley_graph, gen_cayley=tuple(elements), **kwargs)

	def __call__(self, k, *args):
		acc = k
		for x in args:
			if x not in self.elements:
				raise ValueError(f"{x} is not in the semigroup")
			acc = self.product(acc, x)
		return acc

	def cartesian_product(self, other):
		def product(x, y):
			return (self(x[0], y[0]), other(x[1], y[1]))
		return super().cartesian_product(other, product)

	def adjoining_zero(self):
		zero = object()
		def product(x, y):
			if x is zero or y is zero:
				return 0
			return self(x, y)
		return super().add_element(zero, product), zero

	def add_identity(self):
		identity = object()
		def product(x, y):
			if x is identity:
				return y
			if y is identity:
				return x
			return self(x, y)
		return super().add_element(identity, product), identity

	def sub(self, filter_fct):
		return super().sub(filter_fct, self)

	@property
	def idempotents(self):
		yield from filter(lambda e:self(e, e) == e, self)

	def quotient(self, quotient_map):
		"""
			The quotient_map should be a congruence.
			If it is not the case, the resulting
			object will not be a semigroups.
			No checking is done (it is not doable...)

			quotient_map map elements to a representent which is in self.
		"""
		def product(x, y):
			return quotient_map(self(x, y))
		return super().quotient(quotient_map, product)

	def reverse(self):
		return self.__class__(self, product=lambda x, y:self(y, x))

	def left_ideal(self, element):
		def gen():
			seen = set()
			for x in self:
				y = self(element, x)
				if y not in seen:
					yield y
					seen.add(y)
		return self.__class__(gen, self)

	def right_ideal(self, element):
		def gen():
			seen = set()
			for x in self:
				y = self(x, element)
				if y not in seen:
					yield y
					seen.add(y)
		return self.__class__(gen, self)

	@property
	def R_classes(self):
		if not  "_R" in dir(self):
			self._R = nx.condensation(self.cayley_graph)
		return [list(self._R.nodes[x]["members"])[0] for x in self._R.nodes.keys()]

	def R_class_of(self, element):
		self.R_classes
		return self._R.nodes[self._R.graph["mapping"][element]]["members"]

	@property
	def L_classes(self):
		if not  "_L" in dir(self):
			self._L = nx.condensation(self.left_cayley_graph)
		return [list(self._L.nodes[x]["members"])[0] for x in self._L.nodes.keys()]

	def L_class_of(self, element):
		self.L_classes
		return self._L.nodes[self._L.graph["mapping"][element]]["members"]

	@property
	def H_classes(self):
		if not "_H" in dir(self):
			self.R_classes
			self.L_classes
			R = self._R.graph["mapping"]
			L = self._L.graph["mapping"]
			H = { x: (R[x], L[x]) for x in R }
			H_rev =  { t: self._R.nodes[t[0]]["members"].intersection(self._L.nodes[t[1]]["members"]) for t in set(H.values())}
			self._H = H
			self._H_rev = H_rev
		return [ list(t)[0] for t in self._H_rev.values()]

	def H_class_of(self, element):
		self.H_classes
		return self._H_rev[self._H[element]]

	@property
	def D_classes(self):
		if not "_D" in dir(self):
			self.R_classes
			self.L_classes
			D1 = nx.condensation(nx.compose(self.cayley_graph,self.left_cayley_graph))
			D2 = nx.algorithms.dag.transitive_reduction(D1)
			self._D = nx.compose(D2,nx.create_empty_copy(D1))
		return [list(self._D.nodes[x]["members"])[0] for x in self._D.nodes.keys()]

	def D_class_of(self, element):
		self.D_classes
		return self._D.nodes[self._D.graph["mapping"][element]]["members"]

	def iter_R_classes(self):
		self.R_classes #to assure precomputation.
		it = nx.algorithms.topological_sort(self._R)
		yield from (self._R.nodes[i]["members"] for i in it)

	def iter_L_classes(self):
		self.L_classes #to assure precomputation.
		it = nx.algorithms.topological_sort(self._L)
		yield from (self._L.nodes[i]["members"] for i in it)

	def iter_D_classes(self):
		self.D_classes #to assure precomputation.
		it = nx.algorithms.topological_sort(self._D)
		yield from (self._D.nodes[i]["members"] for i in it)

	def _compute_shape_D(self):
		sources = [n for n in self._D.nodes if self._D.in_degree(n) == 0]
		level = {n:0 for n in self._D.nodes}
		lmax = [0]
		def rec_DFS(n, C=0):
			level[n] = max(C,level[n])
			if C > lmax[0]:
				lmax[0] = C
			for m in self._D[n]:
				rec_DFS(m,C+1)
		for n in sources:
			rec_DFS(n)
		shape = {l:[] for l in range(lmax[0] +1)}
		for (n,l) in level.items():
			nb_R = len({self._R.graph["mapping"][x] for x in self._D.nodes[n]["members"]})
			nb_L = len({self._L.graph["mapping"][x] for x in self._D.nodes[n]["members"]})
			shape[l].append((n,nb_R,nb_L))
		heights = {}
		widths = {}
		height_tot = lmax[0]
		width_tot = 0
		for l in range(lmax[0]+1):
			w = len(shape[l])-1
			h = 0
			for (n,nb_R,nb_L) in shape[l]:
				h = max(h,nb_R)
				w += nb_L
			widths[l] = w
			heights[l] = h
			width_tot = max(width_tot,w)
			height_tot += h
		return shape, lmax[0], height_tot+2, width_tot+2, heights, widths

	def _get_H_string(self, th):
		string = ""
		for h in th:
			if h in self.idempotents:
				string += str(h)[1:-1] + "*    "
			else:
				string += str(h)[1:-1] + "    "
		return string[:-4]

	def _get_H_structure(self,n):
		R = list({self._R.graph["mapping"][x] for x in self._D.nodes[n]["members"]})
		L = list({self._L.graph["mapping"][x] for x in self._D.nodes[n]["members"]})
		i = 0
		j = 0
		H = [[0] * len(L) for _ in range(len(R))]
		for l in L:
			for r in R:
				H[i][j] = self._get_H_string([h for (h,(rh,lh)) in self._H.items() if (rh,lh)==(r,l)])
				i+=1
			j += 1
			i = 0
		return H

	@property
	def eggbox(self):
		if not hasattr(self, "_eggbox"):
			self._eggbox = eggbox(self)
		return self._eggbox

	def draw_eggbox(self, filepath, repr_el=None):
		"""
		Use graphviz to compute a eggbox diagram for the semigroup.
		save the image to filepath.
		repr_el is either None, the string "normalize" or a custom function mapping elements
		of the semigroup to representent.
		The "normalize" attribute will map element to a fixed number to avoid encoding issue and display the
		diagram never the less.
		"""
		self.eggbox.draw_graphviz(filepath=filepath, repr_el=repr_el)

	def show_eggbox(self, savefig=None, repr_el=None):
		return self.eggbox.show_graphviz(savefig=savefig, repr_el=repr_el)

	def precompute_greens_relation(self):
		self.D_classes, self.H_classes, self.R_classes, self.L_classes

	def subsemigroup_generated_by(self, elements):
		return self.generated_by(elements, self.product)

	def power(self, x, n):
		if n == 0:
			raise ValueError(f"Undefined exponent in semigroup {x}^{n}")
		if n == 1:
			return x
		y = self.power(x, n//2)
		z = self(y, y)
		if n % 2 == 1:
			z = self(z, x)
		return z


def cache_memb(fct):
	def _fct(self):
		if not fct in self._cache_memb:
			self._cache_memb[fct] = fct(self)
		return self._cache_memb[fct]
	return _fct

class FiniteSemigroup(Semigroup, FiniteUniverse):
	def __init__(self, elements, product=None, contains=None, cayley_graph=None, left_cayley_graph=None, gen_cayley=None):
		if type(elements) is types.FunctionType:
			elements = set(elements())
		Semigroup.__init__(self, elements, product=product, contains=contains, cayley_graph=cayley_graph, left_cayley_graph = left_cayley_graph, gen_cayley = gen_cayley)
		self._cache_memb = {}

	@property
	def idempotents(self):
		if not getattr(self, "_idempotents", False):
			self._idempotents = set(filter(lambda e:self(e, e) == e, self))
		yield from self._idempotents

	def idempotent_power(self, element):
		x = y = element
		n = 1
		while x != self(x, x):
			x = self.power(x, n)
			n += 1
		return x

	def is_monoid(self):
		maxD = next(iter(self.iter_D_classes()))
		try:
			e = next(iter(maxD.intersection(self.idempotents)))
		except StopIteration:
			return False
		for m in self:
			if self(e, m) != self(m, e):
				return False
		return True

	def stable_elements(self, e):
		"""
		e should be an idempotent
		Return elements x such that e+x+e = e
		"""
		if self(e, e) != e:
			raise ValueError(f"{e} should be an idempotent")
		return [y for y in self if self(e, y, e) == e]

	def is_SG(self):
		seen = set()
		for x in self:
			e = self.idempotent_power(x)
			x = self(e, x)
			if x in seen:
				continue
			seen.add(x)
			for y in self:
				if self(x, y, e) != self(e, y, x):
					return False
		return True

	def is_ZG(self):
		seen = set()
		for x in self:
			e = self.idempotent_power(x)
			x = self(e, x)
			if x in seen:
				continue
			seen.add(x)
			for y in self:
				if self(x, y) != self(y, x):
					return False
		return True

	def is_ZE(self):
		for e in self.idempotents:
			for x in self:
				if self(x, e) != self(e, x):
					return False
		return True

	def is_zero(self, element):
		for x in self:
			if self(element, x) != element or self(x, element) != element:
				return False
		return True

	def is_NIL(self):
		E = list(self.idempotents)
		return len(E) == 1 and self.is_zero(E[0])


	def is_subsemigroup(self, E):
		E = set(E)
		return all(self(x, y) in E for x in E for y in E)


	@cache_memb
	def is_aperiodic(self):
		return all(len(self.H_class_of(e)) == 1 for e in self.idempotents)

	is_H_trivial = is_aperiodic
	is_Ap = is_aperiodic

	@cache_memb
	def is_DS(self):
		"""
		The following check that all regular D-class contains only regular H-classes,
		which is equivalent to DS.
		TODO: Add reference for that (see MPRI notes)
		"""
		idempotents = set(self.idempotents)
		while idempotents:
			e = idempotents.pop()
			D = self.D_class_of(e)
			idempotents.difference_update(D)
			if not all(self.idempotent_power(d) in D for d in D):
				return False
		return True

	@cache_memb
	def is_DA(self):
		return self.is_DS() and self.is_aperiodic()

	@cache_memb
	def is_semilattice(self):
		return self.is_commutative() and self.is_idempotent()

	is_Jun = is_semilattice

	@cache_memb
	def is_G(self):
		"""
		A semigroup with only one H-class is necessary a group.
		"""
		return len(self.H_classes) == 1

	is_group = is_G

	@cache_memb
	def is_commutative(self):
		return all(self(x, y) == self(y, x) for x in self for y in self)

	is_Com = is_commutative

	@cache_memb
	def is_abelian(self):
		return self.is_G() and self.is_commutative()

	is_Ab = is_abelian

	@cache_memb
	def is_D_trivial(self):
		return all(len(self.D_class_of(D)) == 1 for D in self.D_classes)

	is_J_trivial = is_D_trivial
	is_J = is_D_trivial

	@cache_memb
	def is_acom(self):
		return self.is_aperiodic() and self.is_commutative()

	is_aperiodic_commutative = is_acom
	is_ACom = is_acom

	@cache_memb
	def is_trivial(self):
		return len(self) <= 1

	@cache_memb
	def is_R_trivial(self):
		return all(len(self.R_class_of(R)) == 1 for R in self.R_classes)

	is_R = is_R_trivial

	@cache_memb
	def is_L_trivial(self):
		return all(len(self.L_class_of(L)) == 1 for L in self.L_classes)

	is_L = is_L_trivial

	@cache_memb
	def is_idempotent(self):
		return all(self(x, x) == x for x in self)

	@cache_memb
	def is_SL(self):
		return self.is_commutative() and self.is_idempotent()

	is_Jun = is_SL

	@cache_memb
	def local_monoids(self):
		loc = []
		Monoid = compAut.algebra.FiniteMonoid
		for e in self.idempotents:
			E = set([self(e, x, e) for x in self])
			loc.append(Monoid(E, product=self, identity=e))
		return loc

	@cache_memb
	def is_locally_DA(self):
		return all(M.is_DA() for M in self.local_monoids())

	is_LDA = is_locally_DA

	@cache_memb
	def is_locally_DS(self):
		return all(M.is_DS() for M in self.local_monoids())

	is_LDS = is_locally_DS

	@cache_memb
	def is_locally_R_trivial(self):
		return all(M.is_R_trivial() for M in self.local_monoids())

	is_LR = is_locally_R_trivial

	@cache_memb
	def is_locally_L_trivial(self):
		return all(M.is_L_trivial() for M in self.local_monoids())

	is_LL = is_locally_L_trivial

	@cache_memb
	def is_locally_J_trivial(self):
		return all(M.is_J_trivial() for M in self.local_monoids())

	is_LJ = is_locally_J_trivial

	@cache_memb
	def is_locally_Com(self):
		return all(M.is_commutatif() for M in self.local_monoids())

	is_LCom = is_locally_Com

	@cache_memb
	def is_locally_Ab(self):
		return all(M.is_abelian() for M in self.local_monoids())

	is_LAB = is_locally_Ab

	@cache_memb
	def is_locally_semilattice(self):
		return all(M.is_semilattice() for M in self.local_monoids())

	is_LJun = is_locally_semilattice

	@cache_memb
	def is_locally_trivial(self):
		return all(M.is_trivial() for M in self.local_monoids())

	is_LI = is_locally_trivial

	@cache_memb
	def is_locally_group(self):
		return all(M.is_group() for M in self.local_monoids())

	is_LG = is_locally_group

	@cache_memb
	def is_locally_ZG(self):
		return all(M.is_ZG() for M in self.local_monoids())

	is_LZG = is_locally_ZG
