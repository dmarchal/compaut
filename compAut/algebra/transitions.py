from compAut.algebra import FiniteSemigroup, FiniteMonoid, Stamp
from compAut.metas import allIn, allExcept
from collections import defaultdict
class TransitionActionBase:
	"""
	Tuple of tuple encoding the transitions action of each letter.
	"""
	_representent = {} #mapping from TransitionAction  instances to their first met instance rep.
	_automaton = None
	def __init__(self, action, rep):
		self.action = action
		self.size = len(action)
		self._hash = hash(self.action)
		if self in self._representent:
			self._rep = self._representent[self]
		else:
			self._rep = self._representent[self] = rep
	def __add__(self, other):
		assert len(self.action) == len(other.action)
		t = tuple( frozenset(k for j in self.action[i] for k in other.action[j]) for i in range(self.size) )
		return type(self)(t, self.rep + other.rep)
	def __hash__(self):
		return self._hash
	def __repr__(self):
		return '·'.join(map(str, self.rep)).replace("\t","\\t").replace("\n", "\\n").replace("\r","\\r").replace(" ", "\\s")
	def __eq__(self, other):
		return hash(self) == hash(other)
	@property
	def rep(self):
		return self._rep

def AutomatonToActions(Automaton, empty_word=None, identity=False):
	states = tuple(Automaton.iter_states())
	states_rev = {s:i for i,s in enumerate(states)}
	n = len(states)
	Letters = [l[0] for s in states for l in Automaton._transitions[s]]
	Letters = list(Letters[0].refine_partition(*Letters[1:]))
	remain = Letters[0].union(*Letters[1:]).complement()
	TransitionAction = type(f"TransitionAction_{id(Automaton)}", (TransitionActionBase,), {"_representent":{}, "_automaton":Automaton})
	if not Letters:
		raise ValueError("Invalid Automaton format")
	if remain:
		Letters.append(remain)
	if empty_word is None:
		empty_word = ""
	if remain:
		Default = TransitionAction(tuple(frozenset(states_rev[s] for s in Automaton.get_successors(states[i], remain)) for i in range(n)), ("⊥",))
		generators = defaultdict(lambda : Default)
		generators[remain] = Default
	else:
		generators = {}
	if identity:
		identity = TransitionAction(tuple(frozenset((i,)) for i in range(n)), rep=("1",))
		generators[empty_word] = identity
	for l in Letters:
		if isinstance(l, allIn):
			rep = next(iter(l))
		else:
			rep = "⊥"
		action = TransitionAction(tuple(frozenset(states_rev[s] for s in Automaton.get_successors(states[i], l)) for i in range(n)), (rep,))
		generators[l] = action
	return generators, empty_word

def TransitionSemigroup(Automaton):
	generators, empty_word = AutomatonToActions(Automaton)
	return FiniteSemigroup.generated_by(generators.values())

def TransitionMonoid(Automaton, empty_word=None):
	generators, empty_word = AutomatonToActions(Automaton, empty_word=empty_word, identity=True)
	return FiniteMonoid.generated_by(generators.values(), identity=generators[empty_word])

def TransitionStamp(Automaton, empty_word=None):
	generators, empty_word = AutomatonToActions(Automaton, empty_word=empty_word, identity=True)
	M = FiniteMonoid.generated_by(generators.values(), identity=generators[empty_word])
	return Stamp(M, generators, empty_word)
