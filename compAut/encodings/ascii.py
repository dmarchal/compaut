import json
import compAut
import string

def encode(chaine):
	return frozenset(chaine.encode())

class ASCII:
	alarm = bytes.fromhex("07")[0]
	escape = bytes.fromhex("1B")[0]
	form_feed = bytes.fromhex("0C")[0]
	line_feed = bytes.fromhex("0A")[0]
	carriage_return = bytes.fromhex("0D")[0]
	tab = bytes.fromhex("09")[0]
	space = bytes.fromhex("20")[0]

	digits = encode(string.digits)
	horizontal_whitespace = frozenset([line_feed, tab, space])
	vertical_whitespace = frozenset([line_feed, carriage_return, form_feed])
	whitespace = encode(string.whitespace)
	word_char = encode(string.ascii_letters)

	@staticmethod
	def __call__(char):
		v = char.encode()
		if v[0] > 127:
			raise ValueError(f"Invalid encoding. {char} is not ASCI")
		return v[0]
		
