import abc
class EncodingApi:
	"""
	Encode interface to encoding information.
	Every characters encoding implementing abstract methods
	will be compile correctly.

	Each abstract methods should returns a set of bytestrings or a bytestring.
	"""
	@abc.abstractmethod
	def	decimal_digits(self):
		pass

	@abc.abstractmethod
	def horizontal_whitespace(self):
		pass

	@abc.abstractmethod
	def vertical_whitespace(self):
		pass

	@abc.absractmethod
	def whitespace(self):
		pass

	@abc.abstractmethod
	def word_char(self):
		pass

	# Following return bytestring #
	@abc.abstractmethod
	def alarm(self):
		pass

	@abc.abstractmethod
	def escape(self):
		pass

	@abc.abstractmethod
	def form_feed(self):
		pass

	@abc.abstractmethod
	def line_feed(self):
		pass

	@abc.abstractmethod
	def carriage_return(self):
		pass

	@abc.abstractmethod
	def tab(self):
		pass

