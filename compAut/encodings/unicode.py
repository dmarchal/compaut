import json
import compAut
from .ascii import ASCII

"""
Contains various definitions of unicode categories.
Data source: https://www.unicode.org/Public/13.0.0/ucd/

The UnicodeData class is an interface to some static (json) files
used in the project.
"""

path = f'{compAut.__path__[0]}/encodings/unicode_data/'

class Unicode:
	version = 13
	data_source = "https://www.unicode.org/Public/13.0.0/ucd/"
	@property
	def category(self):
		if not hasattr(self, "_category"):
			with open(f"{path}categories.json") as f:
				self._category = {e:tuple(map(str.encode, L)) for e, L in json.load(f).items()}
		return self._category

	def category_description(self, cat):
		if not hasattr(self, "_category_descr"):
			with open(f"{path}cat_descr.json") as f:
				self._category_descr = json.load(f)
		if cat not in self._category_descr:
			raise ValueError(f"{cat} is not a valid unicode category abbreviations")
		return self._category_descr[cat]

	@property
	def category_list(self):
		if not hasattr(self, "_cat_descr"):
			with open(f"{path}cat_descr.json") as f:
				self._category_desc = json.load(f)
		return list(self._category_desc)

	@property
	def horizontal_whitespace(self):
		return frozenset(self.category["Zs"])

	@property
	def vertical_whitespace(self):
		return ASCII.vertical_whitespace.union(self.category["Zp"], self.category["Zl"], [hexa_encode("0085")])

	@property
	def whitespace(self):
		return self.horizontal_whitespace().union(self.vertical_whitespace())

	@property
	def letters(self):
		result = frozenset()
		for c in ("Lu", "Ll", "Lt", "Lm", "Lo"):
			result = result.union(self.category[c])
		return result

	@property
	def numbers(self):
		result = frozenset()
		for c in ("Nd", "Nl", "No"):
			result = result.union(self.category[c])
		return result

	@property
	def decimal_digits(self):
		return self.category["Nd"]

	@property
	def word_char(self):
		return self.numbers.union(self.letters)

	# Following return bytestring #
	@property
	def alarm(self):
		return ASCII.alarm

	@property
	def escape(self):
		return ASCII.escape

	@property
	def form_feed(self):
		return ASCII.form_feed

	@property
	def line_feed(self):
		return ASCII.line_feed

	@property
	def carriage_return(self):
		return ASCII.carriage_return

	@property
	def tab(self):
		return ASCII.tab
