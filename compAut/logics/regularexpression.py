import lark
from compAut.models.finiteautomata import Automaton
from lark.tree import Tree
from compAut.parser.regexp_transformer import ClassicalTransformer, PCRETransformer
import compAut

__all__ = ['ClassicalRegularExpression', "PCRERegularExpression"]

class AbstractRegExp:
	grammar = None
	transformer = None
	def __init__(self):
		with open(self.grammar) as f:
			self.parser = lark.Lark(f.read())
	def get_parse_tree(self, pattern):
		return self.parser.parse(pattern)

	def compile(self, pattern):
		return self.transformer().transform(self.get_parse_tree(pattern))

class ClassicalRegularExpression(AbstractRegExp):
	grammar = f'{compAut.__path__[0]}/parser/classic_regexp.lark'
	transformer = ClassicalTransformer

class PCRERegularExpression(AbstractRegExp):
	grammar = f'{compAut.__path__[0]}/parser/pcre.lark'
	transformer = PCRETransformer
