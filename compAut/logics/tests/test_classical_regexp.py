from compAut.logics import cre

def test_cre_blogpost():
	"""
	Following examples extracted (and freely adapted) from :
	https://code.tutsplus.com/tutorials/8-regular-expressions-you-should-know--net-6149
	Start and end string methods removed.
	"-" in set get escaped "\-"
	"""
	re1 = cre.compile("[a-z0-9_\-]{3,16}") # Why is is so slow :'(
	assert "my-us3r_n4m3" in re1
	assert not "th1s1s-wayt00_l0ngt0beausername" in re1

	re2 = cre.compile("[a-z0-9_\-]{6,18}")
	assert "myp4ssw0rd" in re2
	assert not "mypa$$w0rd " in re2

	re3 = cre.compile("#([A-F0-9]{6}|[a-f0-9]{6})")
	assert "#a3c113" in re3
	assert "#A3C113" in re3
	assert "#a3C113" not in re3 # mixing lower and upper case not allowed
	assert "#a3G113" not in re3
	assert "#a3C1130" not in re3

	re4 = cre.compile("([a-z0-9_\.\-]+)@([a-z\.\-]+)\.([a-z\.]{2,6})")
	assert "john@doe.com" in re4
	assert " john@doe.something" not in re4


def test_regexp_blogpost2():
	"""
	https://rust-leipzig.github.io/regex/2017/03/28/comparison-of-regex-engines/
    	Twain
    	(?i)Twain
    	[a-z]shing
    	Huck[a-zA-Z]+|Saw[a-zA-Z]+
    	\b\w+nn\b
    	[a-q][^u-z]{13}x
    	Tom|Sawyer|Huckleberry|Finn
    	(?i)Tom|Sawyer|Huckleberry|Finn
    	.{0,2}(Tom|Sawyer|Huckleberry|Finn)
    	.{2,4}(Tom|Sawyer|Huckleberry|Finn)
    	Tom.{10,25}river|river.{10,25}Tom
    	[a-zA-Z]+ing
    	\s[a-zA-Z]{0,12}ing\s
    	([A-Za-z]awyer|[A-Za-z]inn)\s
    	["'][^"']{0,30}[?!\.][\"']
    	\u221E|\u2713
    	\p{Sm}

	"""
	#TODO: tests regexp above from the blogpost
	pass



def test_basic_operation():
	B = cre.compile("ab")
	assert "ab" in B
	assert "aba" not in B

	C = cre.compile("ab?")
	assert "a" in C
	assert "ab" in C
	assert "ac" not in C

	D = cre.compile("a|b")
	assert "a" in D
	assert "b" in D
	assert "c" not in D

	E = cre.compile("(ab)*")
	assert "ababab" in E
	assert "abababa" not in E
	assert E.is_equivalent_to(B.kleene_star())

	F = cre.compile("a{5}")
	assert "a"*5 in F
	assert "b" not in F
	assert "a"*6 not in F
	assert "a"*4 not in F

	G = cre.compile("a{5,10}")
	assert "a"*5 in G
	assert "b" not in G
	assert "a"*6 in G
	assert "a"*16 not in G
	assert "a"*4 not in G

	H = cre.compile("a{5,}")
	assert "a"*5 in H
	assert "b" not in H
	assert "a"*6 in H
	assert "a"*16 in H
	assert "a"*4 not in H


def test_concatenation_optional_operation():
	A = cre.compile("a*b?")
	assert "a"*5 in A
	assert "" in A
	assert "b" in A
	assert "ab" in A
	assert "aaaab" in A
	assert not "abb" in A

def test_priority():
	"""
	Unary operator has a priority 2
	concatenation as a priority 1
	binary operator (|, -) has a priority 0
	"""
	Equivalence = {
		"ab*" : "a(b*)",
		"ab|c": "(ab)|c",
		"ab?" : "a(b?)",
		"a|b" : "b|a",
		"[ab]": "[ba]"
	}
	for r,t in Equivalence.items():
		r,t = cre.compile(r), cre.compile(t)
		assert r.is_equivalent_to(t)

def test_set_complement():
	A = cre.compile("[^a]")
	assert "a" not in A
	assert "b" in A
	assert (3,) in A

	B = cre.compile("[\^a]")
	assert "a" in B
	assert "^" in B
	assert "b" not in B

def test_snort():
	pass
