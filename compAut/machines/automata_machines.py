from compAut.metas import allIn, allExcept, metaLetter, metaObject
from compAut.logics.regularexpression import ClassicalRegularExpression
from .line_machines import SimpleLineMachine as LineMachine
import pickle
import base64
cre = ClassicalRegularExpression()

class GrepLikeAutomaton:
	"""
	Match the expression line by line.
	"""
	descr = ""
	pyreload = lambda self: f"""
To restore the python Automaton used in this program, execute in Python:

with open('path/to/obj.dump') as f:
	M = {self.__module__}.{self.__class__.__name__}.loads(f.read())

The automaton (or the underlying expression if provided) can be access through:
	M.expression # Exists only if the machine was build through a regexp.
	M.automaton # Always exists

To recompile, executes:
M.compile(name='{self.name}')
"""
	def __init__(self, to_execute, minimize=True, name=None):
		"""
		to_execute can be either a deterministic Automaton or a string that will be compiled
		to regular expression.

		The default behavior is to minimize and normalize the automaton but you can prevent that by
		setting minimize to False.

		Provides dumping and loading facilities using pickle and dump64.
		"""
		self.data_state = {"to_execute" : to_execute, "minimize" : minimize, "name":name}
		self.expression = None
		if isinstance(to_execute, str):
			self.expression = to_execute
			to_execute = cre.compile(to_execute)
		if name is not None:
			self.name = name
		automaton = to_execute
		if minimize:
			automaton = automaton.concatenate(cre.compile("\n?")).expanded_utf8_chars().minimize()
		self.automaton = automaton

	def dumps(self):
		x = base64.encodebytes(pickle.dumps(self.data_state))
		return x.decode().replace("\n","")

	@classmethod
	def loads(cls, s):
		d = pickle.loads(base64.decodebytes(s.encode()))
		return cls(**d)
	def __repr__(self):
		if self.expression is not None:
			expr = self.expression.replace("\\","\\\\")
			infos = f"The automaton was build from the expression `{expr}`"
		else:
			infos = "The automaton was directly provided."
		return f"a Grep Like automaton.\n{infos}\n{self.descr}"

class TabulatedGrepLikeAutomaton(GrepLikeAutomaton, LineMachine):
	"""
	A tabulated version.
	The automaton is represented by int **transitions with the transition
	(source, letter, target) being:
		transitions[source][letter]
	Each letter being a char, each transitions[state] is an array of size 255.
	Size can growth quickly for big automaton.
	"""

	name = "GrepLikeTab"
	descr = """a finite automaton executed line by line (through repetive calls to `getline` in `stdio.h`.
The transitions table is a two dimensional static array (hence the name, tab for tabulated)".
"""

	def header(self):
		transitions = []
		A = self.automaton
		size = len(A.states)
		stype = "int"
		if size < 255:
			stype = "unsigned char"
		s = f"static const {stype} transitions[{size}][256] = {{\n\t\t"
		for i in range(size):
			transitions.append(f"{{ {', '.join(map(str, tuple(next(iter(A.get_successors(i, j))) for j in range(256))))} }}")
		return s + ',\n\t\t'.join(transitions) + "};"

	def reinit(self):
		return f"state = {next(iter(self.automaton.initial_states))};"
	def main_loop(self):
		return "state = transitions[state][c];"
	def accepting (self):
		return " || ".join(f"state=={i}" for i in self.automaton.final_states)


class SwitchLineCompilableMachine(LineMachine):
	def main_loop(self):
		return f"""
			switch (state){{
				{self.switch()}
			}}
		"""

class SwitchGrepLikeAutomaton(GrepLikeAutomaton, SwitchLineCompilableMachine):
	"""
	Will implement the transition table using a switch statement and with
	block of if then ... else if  ...
	"""
	name = "swGrepLike"
	descr = "The transitions table is implemented using a switch over states of conditional statements."
	def reinit(self):
		return f"state={next(iter(self.automaton.initial_states))};"

	def accepting (self):
		return " || ".join(f"state=={i}" for i in self.automaton.final_states)

	def switch(self):
		tr = self.automaton._transitions
		switches = []
		for s in tr:
			letters = list(tr[s].keys())
			if len(letters) == 1 and s in tr[s][letters[0]]:
				continue # if s is a sinkstate we don't do anything actually
			switches.append(f"case {s}:")
			for i, l in enumerate(letters):
				target = next(iter(tr[s][l]))
				if not i:
					switches.append(f"\tif ({self.metaletters_to_conditions(l)}) state = {target};")
					continue
				switches.append(f"\telse if ({self.metaletters_to_conditions(l)}) state = {target};")
			switches.append("break;")
		return "\n\t\t\t\t".join(switches)


	def metaletters_to_conditions(self, letter, threshold=5):
		"""
		We decompose a meta letters disjunction of equality if they are less than threshold,
		otherwise, we check for ordered partition. This could probably improve a lot.
		"""
		if isinstance(letter, tuple):
			assert len(letter) == 1
			letter = letter[0]
		if isinstance(letter, metaObject):
			letter = letter[0]
		if isinstance(letter, allExcept):
			return f"!({self.metaletters_to_conditions(letter.complement())})"
		if isinstance(letter, metaLetter):
			l = len(letter._fset)
		else:
			l = len(letter)
		if l == 0:
			if isinstance(letter, allIn):
				return "0"
		if l < threshold:
			return f'{" || ".join(f"c=={l}" for l in letter)}'
		m = min(letter)
		M = max(letter)
		covers = set(range(m, M+1))
		errors = covers.difference(letter)
		if len(errors) > len(covers)/threshold:
			merror = min(errors)
			return f"(({self.metaletters_to_conditions(letter.intersection(range(m, merror)), threshold=threshold)}) || ({self.metaletters_to_conditions(letter.intersection(range(merror, M+1)), threshold=threshold)}))"
		res = f"c>={m} && c <= {M}"
		if errors:
			res += f" && !({self.metaletters_to_conditions(errors, threshold=threshold)})"
		return res
