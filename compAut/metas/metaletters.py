import abc
from compAut.utils import shorten

class metaLetter(abc.ABC):
	"""
	A set or a coset of object. The underlying datastructure
	is a frozenset.
	Repr is at most of size 19
	"""
	def __init__(self, *args):
		f = set()
		args = ((e,) if "__iter__" not in dir(e) else e for e in args)
		f.update(*args)
		self._fset = frozenset(f)

	def __iter__(self):
		return iter(self._fset)

	@abc.abstractmethod
	def __contains__(self, y):
		pass

	@abc.abstractmethod
	def union(self, *others):
		pass

	@abc.abstractmethod
	def intersection(self, *others):
		pass

	@abc.abstractmethod
	def complement(self):
		pass

	def difference(self, other):
		if not isinstance(other, metaLetter):
			other = allIn(other)
		return self.intersection(other.complement())

	def symetric_difference(self, other):
		if not isinstance(other, metaLetter):
			other = allIn(other)
		return self.difference(other).union(other.difference(self))

	def __repr__(self):
		if "_repr" not in dir(self):
			return self.repr()
		return self._repr

	def repr(self, size=None):
		content = shorten(','.join(map(str, sorted(map(str, self)))))
		return f"{{{content}}}"


	def refine_partition(self, *others):
		"""
		Take a bunk of metaletters (self, *others)
		and return refine partition of the set they cover.
		"""
		if len(others) == 0:
			yield self
			return
		other = others[0]
		if not isinstance(other, metaLetter):
			other = allIn(other)
		others = set(others[1:]) # remove repetitions
		meta = self
		for atom in other.refine_partition(*others):
			m = meta.intersection(atom)
			m = meta.intersection(atom)
			if m:
				yield m
				meta = meta.difference(m)
			m = atom.difference(m)
			if m:
				yield m
				meta = meta.difference(m)
		if meta:
			yield meta

	def __eq__(self, other):
		if not isinstance(other, metaLetter):
			return False
		return hash(self) == hash(other)

	def is_all(self):
		return self == allExcept()

	def is_empty(self):
		return self == allIn()


class allIn(metaLetter):
	def __contains__(self, y):
		return self._fset.__contains__(y)

	def __len__(self):
		return len(self._fset)

	def intersection(self, *others):
		if len(others) == 0:
			return self
		other = others[0]
		others = others[1:]
		if isinstance(other, allExcept):
			return allIn(self._fset.difference(other._fset)).intersection(*others)
		return allIn(self._fset.intersection(other)).intersection(*others)

	def union(self, *others):
		if len(others) == 0:
			return self
		other = others[0]
		if not isinstance(other, metaLetter):
			other = allIn(other)
		other = other.union(*others[1:])
		if isinstance(other, allExcept):
			return allExcept(other._fset.difference(self._fset)).union(*others)
		return allIn(self._fset.union(other))

	def complement(self):
		return allExcept(self._fset)

	def __bool__(self):
		return bool(self._fset)

	def __hash__(self):
		if "_hash" not in dir(self):
			self._hash = hash((0, frozenset(self)))
		return self._hash

class allExcept(metaLetter):
	"""
	Meta Letter representing all letters except those in the set
	"""

	def __contains__(self, y):
		return not self._fset.__contains__(y)

	def __bool__(self):
		return True

	def intersection(self, *others):
		others = map(lambda other: allExcept(other) if not isinstance(other, metaLetter) else other.complement(), others)
		return self.complement().union(*others).complement()

	def union(self, *others):
		others = map(lambda other: allExcept(other) if not isinstance(other, metaLetter) else other.complement(), others)
		return self.complement().intersection(*others).complement()

	def complement(self):
		return allIn(self._fset)

	def __repr__(self):
		if "_repr" not in dir(self):
			r = super().__repr__()
			self._repr = f"{r}ᶜ"
		return self._repr

	def __hash__(self):
		if "_hash" not in dir(self):
			self._hash = hash((1, frozenset(self)))
		return self._hash
