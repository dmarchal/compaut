from compAut.metas import allIn, allExcept
def test_metaletter_init():
	ab = allIn("ab")
	assert ab == allIn("ba")
	assert ab == allIn(set("ab"))
	assert ab == allIn(ab)

	nab = allExcept("ab")
	assert nab == allExcept("ab")
	assert nab == allExcept(set("ab"))
	assert nab == allExcept(nab)

	assert ab == allIn(nab)
	assert nab == allExcept(ab)


def test_metaletter_contains():
	ab = allIn("ab")
	assert "a" in ab
	assert "b" in ab
	assert "c" not in ab
	assert 3 not in ab
	assert (3, 3) not in ab
	assert object() not in ab

	nab = allExcept("ab")
	assert "a" not in nab
	assert "b" not in nab
	assert 3 in nab
	assert (3, 3) in nab
	assert object() in nab

	other = allExcept([3, 4, (1,2), "c"])
	assert 3 not in other
	assert (1, 2) not in other
	assert "d" in other
	assert object() in other

def test_boolean_operation():
	ab = allIn("ab")
	nab = allExcept("ab")
	assert ab.union(nab) == allExcept()
	assert allExcept().complement() == allIn()
	assert allIn().complement() == allExcept()

	assert allIn("a").union("b") == ab
	assert allExcept("a").intersection(allExcept("b")) == nab
	assert nab.intersection(ab) == allIn()
	assert nab.union(ab) == allExcept()
	assert nab == ab.complement()

def test_refine_partition():
	assert set(allIn().refine_partition(allExcept(), allIn("ab"), allIn("bc"))) == {allIn("a"), allIn("b"), allIn("c"), allExcept("abc")}
	assert set(allIn().refine_partition(allExcept(), allIn(range(10)), allIn(range(10)))) == {allExcept(range(10)), allIn(range(10))}



