from compAut.metas import metaTuple, metaTuples, allIn, allExcept
A = allExcept()
def check_mt(t1, t2):
	assert t1.dimension == t2.dimension
	assert t1._equals == t2._equals
	assert t1._uniq_set == t2._uniq_set
	assert t1._not_equals == t2._not_equals
	assert t1._metatuple == t2._metatuple
	assert t1 == t2 # should be true.

def neg_check_mt(t1, t2):
	L = [
		not t1.dimension == t2.dimension,
		not t1._equals == t2._equals,
		not t1._uniq_set == t2._uniq_set,
		not t1._not_equals == t2._not_equals,
		not t1._metatuple == t2._metatuple
		]
	assert any(L)
	assert not t1 == t2 # should be true.

def test_metatuple_new():
	t = metaTuple(("ab", "ab"))
	check_mt(t, metaTuple(("ba", "ba")))
	check_mt(t, metaTuple((("a","b"), allIn("ab"))))

	neg_check_mt(t, metaTuple(("ab", "ab"), equals=[(0, 1)]))
	check_mt(t.equals(0, 1), metaTuple(("ab", "ab"), equals=[(0, 1)]))
	check_mt(t.equals(0, 1), metaTuple(("ab", A), equals=[(0, 1)]))


	t2 = metaTuple(("", ""))
	check_mt(t2, metaTuple(("a", "b"), equals=[(0, 1)]))
	check_mt(t2, metaTuple((allExcept("a"), allIn("a")), equals=[(0, 1)]))
	check_mt(t2, metaTuple((A, A), equals=[(0, 1)], not_equals=[(0, 1)]))

	t3 = metaTuple((allExcept("ab"), allExcept("cd")), equals=[(0, 1)])
	check_mt(t3, metaTuple((allExcept("abcd"), allExcept("abcd")), equals=[(0, 1)]))

	t4 = metaTuple(("", "", "", ""))
	check_mt(t4, metaTuple.empty_tuple(4))
	check_mt(t4, metaTuple((A, A, A, A), equals=[(0, 1), (1, 2), (2, 3)], not_equals=[(0, 3)]))

	t5 = metaTuple(("ab", "ac"), equals=[(0, 1)])
	check_mt(t5, metaTuple(("a", "a")))

def test_metatuple_op():
	pass
