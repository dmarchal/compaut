import abc, itertools, tempfile
import networkx as nx
from compAut.metas import allExcept, allIn, metaLetter, metaTuple, metaTuples, fset, sinkState, freshState, Marker, metaObject
from compAut.utils import show_img, recursive_shallow_dictcopy

class AbstractStatesMachine(abc.ABC):
	"""
	An abstract class to manipulate StatesMachines.
	A StatesMachine is an immutable object.
		Internally it contains a representation of its transitions (self._transitions)
		as:
			states => metaTuples (of metaLetters) => states => action

		The dimension of the machine is the length of the metaTuple .
		E.g. for finite automata the dimension 1, for the stackautomaton it is 2, ect ..

		Action is abstracted away in super-classes.
		Typically, an action can be to produce an output, to change position, ect..

		The first component of the metaTuple is the letter to read, the others are originated from memory management.

		==== Note on metaTuples ===

		The class metaTuple(s) is an abstraction of tuples of python object supporting boolean operations
		among other operations. They allow to designate complex (possibly) infinite set of elements.
		They also support basic (equality) operation among coordinates.


	The instance of AbstractMachine are non-mutable and should
	be construct through operations or class constructors.

	The letters are metaLetter (i.e. frozenset representing finite or
	cofinite objects).

	The class implements:
		* The abstraction for metaLetter and metaTuples resolutions.
		* Expand str transitions into utf8 encoding
		* accessors for transtions table
		* checking determinisms
	"""

	_dimension = None

	@property
	def dimension(self):
		"""
			Property returning the dimension of the machine.
		"""
		return self._dimension

	def _check_fields(self, state):
		if "transitions" not in state:
			raise TypeError(f"{self.__class__} cannot be instantiated without transitions")

	def __setstate__(self, state):
		self._check_fields(state)
		for f, v in state.items():
			setattr(self, f"_{f}", v)

	def __getstate__(self):
		return { "transitions" : recursive_shallow_dictcopy(self._transitions) }

	def __new__(cls, **kwargs):
		self = object.__new__(cls)
		self.__setstate__(kwargs)
		return self

	@classmethod
	def build_from(cls, *instances,  **kwargs):
		"""
		Build a new instance from instances by
		taking from theirs states sequentially
		and finally modifying the resulting state
		with kwargs.

		To copy an instance:
		cls.build_from(instance)
		"""
		state = {}
		for instance in instances:
			state.update(instance.__getstate__())
		state.update(kwargs)
		return cls.__new__(cls, **state)

	@classmethod
	def from_transitions(cls, transitions, *instances, **kwargs):
		"""
		build from instances and **kwargs using transitions fetch from "transitions".

		transitions should be an iterable of either (source, letter, target, actions)
		or (source, letter, target).
		"""
		tr = {}
		for transition in transitions:
			source, letters, target = transition[:3]
			if not letters:
				continue
			action = transition[3] if len(transition) == 4 else None
			if isinstance(letters, metaLetter):
				letters = (letters,)
			if isinstance(letters, metaObject):
				metaT = letters
			else:
				metaT = metaTuple(letters)
			if metaT.dimension != cls._dimension:
				raise ValueError(f"Transitions {transition} invalid number of dimension. Got {metaT.dimension} expected {cls._dimension}")
			d = tr[source] = tr.get(source, {})
			s = d[metaT] = d.get(metaT, {})
			s[target] = action
		kwargs["transitions"] = tr
		return cls.build_from(*instances, **kwargs)

	@property
	def states(self):
		states = getattr(self, "_states", None)
		if not states:
			self._states = fset(sum(((t[0], t[2]) for t in self.iter_transitions()), ()))
		return self._states

	def iter_states(self):
		"""
		Iter through states. Overload to give states
		an order to use in normalize.
		"""
		return iter(self.states)

	def iter_transitions(self):
		"""
		return an iterator of transitions.
		"""
		for source, lmap in self._transitions.items():
			for metaletter_tup, target_map in lmap.items():
				for target, action in target_map.items():
					if action is not None:
						yield (source, metaletter_tup, target, action)
					else:
						yield (source, metaletter_tup, target)
	@property
	def sink_state(self):
		"""
		Produces a fresh state that is stored and can be used as a sink state.
		(see "complete" method for usage)
		"""
		sink = getattr(self, "_sink", None)
		if sink is None:
			self._sink = sinkState()
		return self._sink


	def get_successors(self, state, letter, *inputs):
		"""
		Compute for a state and (meta)letters the mapping:
		an iterable of couple (state, action).

		A letter can be any hashable object or a metaLetter.

		To get all successors by all letters of a given state call
			.get_successors(state, allExcept())
		"""
		if not isinstance(letter, metaObject):
			metaT = metaTuple((letter,)+inputs)
		else:
			metaT = letter

		for meta, successors_map in self._transitions.get(state, {}).items():
			if metaT.dimension != meta.dimension:
				raise ValueError(f"Invalid input, expected tuple of length {meta.dimension} got {metaT.dimension}")
			if meta.intersection(metaT):
				for succ, action in successors_map.items():
					yield (succ, action)

	def get_successors_from(self, states, metaT):
		"""
		Compute the successors for all states in "states"
		with potential dupplicates
		"""
		nextstates = {}
		for state in states:
			yield from self.get_successors(state, metaT)


	def complete(self, **kwargs):
		"""
		Return a "complete" machine. That is a machine where
		state have successors for all letter.
		"""
		sink = self.sink_state
		tr = recursive_shallow_dictcopy(self._transitions)
		for state in self.states:
			t = tuple(self._transitions.get(state, {}).keys())
			if t:
				missing_letters = t[0].union(*t[1:]).complement()
			else:
				missing_letter =  metaTuple.all(self.dimension)
			if missing_letters:
				tr[state] = d = tr.get(state, {})
				d.update({ m: {sink:None} for m in missing_letters })
		return self.build_from(self, transitions=tr, **kwargs)


	def normalize(self, translate=None, **kwargs):
		"""
		Relabel states by following the order provided by iter_states.
		Also merge redundant transitions.
		"""
		translate = { state:i for i, state in enumerate(self.iter_states()) } if translate is None else translate
		pre_tr = { } # (state, action, state) => set of letter for the transitions.
		for transitions in AbstractStatesMachine.iter_transitions(self):
			s, l, t = transitions[:3]
			action = transitions[3] if len(transitions)>3 else None
			d = pre_tr[(s, action, t)] = pre_tr.get((s, action, t), metaTuple.empty_tuple(self.dimension)).union(l)
		transitions = []
		for (i, action, j), t in pre_tr.items():
			t = [translate[i], t, translate[j]]
			if action is not None:
				t.append(action)
			transitions.append(t)
		return self.from_transitions(transitions, self, **kwargs)

	def reverse(self, **kwargs):
		"""
		Reverse the transitions.
		"""
		transitions = [ (t[2], t[1], t[0]) + t[3:] for t in AbstractStatesMachine.iter_transitions(self) ]
		return self.from_transitions(transitions, self, **kwargs)

	def is_deterministic(self):
		for s,t in self._transitions.items():
			if any((len(v) > 1 for v in t.values())):
				return False
		for s, letter_map in self._transitions.items():
			letters = tuple(letter_map.keys())
			for i in range(len(letters)):
				for j in range(i+1, len(letters)):
					if letters[i].intersection(letters[j]):
						return False
		return True

	@property
	def support_alphabet(self):
		"""
		Return the set of letters occuring either positively or negatively
		"""
		S = set()
		S.update(*(x[1][0]._fset for x in self.iter_transitions()))
		return frozenset(S)

	def __repr__(self):
		return f"<{self.__class__.__name__}>"

	def expanded_utf8_chars(self, encode=lambda e:str.encode(e, encoding='utf-8'), **kwargs):
		"""
		Return a state machine where non meta letters are replaced by int values (C char).
		It only concerns first elements of tuple_letters of transitions, the other one being
		inmemory letter and left to the machine as it is.

		It applies the method encode to get the raw binary value of each letter.
		If a given letter does not possess an encode value, it will raise an error.

		Characters encoded with several values like '☂' are expended
		into several transitions introducing fresh states.

		Ultimately, we get an automaton over 255 letters (one for each char).
		Characters encoding used by default is str.encode, but a custom function
		can be provided.

		The special case of allExcept letters requires a particular attention.
		In particular, we need a special actions to consume characters possibly
		encoded with more values.

		The following code is NOT checking that the UTF8 encoding is correct.
		It assume that as soon as it sees an octet whose valued is strictly larger than 127
		the following octets whose valued is larger than 127 are encoding the same letters.
		"""
		alls = allIn(range(256))
		continuation = metaTuple((allIn(range(128, 192)),)+(alls,)*(self.dimension-1))	# Values that follows
		alls = metaTuple((alls,)*self.dimension)
		d = self.dimension
		transitions = []
		for t in AbstractStatesMachine.iter_transitions(self):
			source, metaT, target = t[:3]
			action = t[3] if len(t) == 4 else None
			def encode(letter):
				if isinstance(letter, bytes):
					return letter
				return letter.encode()
			meta = set(tuple(encode(e) for e in metaT.project(0)))
			AllFirst = metaT.all_on(0)
			if len(meta):
				max_len = max(map(len, meta))
			else:
				max_len = 0
			if max_len==1 and isinstance(metaT[0], allIn):
				m = ((e[0] for e in meta),)
				t = metaTuple(m, dimension=d).intersection(AllFirst)
				transitions.append((source, t, target, action))
				continue
			elif isinstance(metaT[0], allIn):
				state = source
				for i in range(max_len):
					m = (allIn(e[i] for e in meta if len(e) == i+1),)
					t = metaTuple(m, dimension=d).intersection(AllFirst)
					transition = (state, t, target, action)
					transitions.append(transition)
					fresh = freshState()
					m = (allIn(e[i] for e in meta if len(e) > i+1),)
					transition = (state, metaTuple(m, dimension=d), fresh)
					transitions.append(transition)
					state = fresh
			elif isinstance(metaT[0], allExcept):
				transition = (target, continuation, target) # Possible continuation to consume.
				transitions.append(transition)
				m = tuple(e[0] for e in meta if len(e) >= 1)
				t = metaTuple((allExcept(m),), dimension=d)
				transition = (source, t, target)
				transitions.append(transition)
				state = source
				for i in range(1, max_len + 1):
					freshstate = freshState()
					m = (allIn(e[i-1] for e in meta if len(e) >= i),)
					t = metaTuple(m, dimension=d)
					transition = (state, t, freshstate)
					transitions.append(transition)
					state = freshstate
					m = ((e[i-1] for e in meta if len(e) == i),)
					t = continuation.intersection(metaTuple(m, dimension=d))
					transition = (state, t, target)
					transitions.append(transition)

			else:
				raise TypeError(f"Wrong metaLetter object {metaT}")
		return self.from_transitions(transitions, self, **kwargs)

	def union(self, other, labels=None, **kwargs):
		if labels is None:
			left, right = Marker("Left"), Marker("Right")
		else:
			left, right = labels
		tr = map(lambda e:((left, e[0]), e[1], (left, e[2])) + e[3:], AbstractStatesMachine.iter_transitions(self))
		tr = itertools.chain(tr, map(lambda e:((right, e[0]), e[1], (right, e[2]))+ e[3:], AbstractStatesMachine.iter_transitions(other)))
		return self.from_transitions(tr, self, other, **kwargs)

	def show(self, save_fig=None):
		if save_fig is None:
			fd, path = tempfile.mkstemp(suffix=".png")
			open(fd).close()
		else:
			path = save_fig
		self.draw(path)
		return show_img(path)

	def _graphviz_transitions(self, G):
		for t in self.iter_transitions():
			source = t[0]
			target = t[2]
			label = (t[1],) + t[3:]
			if self.dimension == 1:
				def crepr(el):
					el = el[0]
					if isinstance(el, allIn) and len(el) == 1:
						return str(next(iter(el)))
					return str(el)
			else:
				crepr = str
			label = ", ".join(map(crepr, label))
			label = label.replace("<", "\<").replace(">", "\>")
			G.add_edge(t[0], t[2], label=label)

	def _graphviz_extra(self, G):
		pass

	def _graphviz_graph(self):
		try:
			import pygraphviz as pyg
		except ModuleNotFoundError:
			raise ModuleNotFoundError("Drawing an automaton requires the module pygraphviz. Install it using `pip3 install -U pygraphviz`")
		G = pyg.AGraph(directed=True)
		self._graphviz_transitions(G)
		self._graphviz_extra(G)
		return	G

	def draw(self, file_path):
		G = self._graphviz_graph()
		G.layout(prog="dot")
		G.draw(file_path)

	@property
	def transition_graph(self):
		"""
			Construct a networkx graph with states as nodes and one edge between state
			if at least one transition connect them. Remove self-loop.
		"""
		if not hasattr(self, "_transition_graph"):
			G = nx.DiGraph()
			G.add_edges_from((s[0], s[2]) for s in self.iter_transitions() if s[0] != s[2])
			self._transition_graph = G
		return self._transition_graph

	def is_acyclic(self):
		"""
			Check that no nontrivial cyclic transitions can be found
			TODO: that
		"""
		return nx.is_directed_acyclic_graph(self.transition_graph)

class RunnableMachine(AbstractStatesMachine):
	"""
	An Abstract State machine with both initial and final states.
	The machine should implement a method run.
	"""
	def __getstate__(self):
		state = super().__getstate__()
		state["initial_states"] = fset(self._initial_states)
		state["final_states"] = fset(self._final_states)
		return state

	def _check_fields(self, state):
		super()._check_fields(state)
		for key in ["initial_states", "final_states"]:
			if key not in state:
				raise TypeError(f"{self.__class__} cannot be instantiated without `{key}` keyword")

	@property
	def initial_states(self):
		return fset(self._initial_states)
	@property
	def final_states(self):
		return fset(self._final_states)

	def _graphviz_extra(self, G):
		for i in self.initial_states:
			G.add_node(f"{i}_start", shape="none", label="")
			G.add_edge(f"{i}_start", i)

		for i in self.final_states:
			G.add_node(f"{i}", shape="doublecircle")

	def normalize(self, translate=None, **kwargs):
		translate = { state:i for i, state in enumerate(self.iter_states()) } if translate is None else translate
		initial_states = set(translate[s] for s in self.initial_states)
		final_states = set(translate[s] for s in self.final_states)
		return super().normalize(translate=translate, initial_states=initial_states, final_states=final_states, **kwargs)

	def reverse(self, **kwargs):
		initial_states, final_states = set(self.final_states), set(self.initial_states)
		return super().reverse(initial_states=initial_states, final_states=final_states, **kwargs)

	def union(self, other, labels=None, **kwargs):
		if labels is None:
			left = Marker("L")
			right = Marker("R")
		else:
			left, right = labels
		initial_states = set( (left, s) for s in self.initial_states )
		initial_states.update((right, s) for s in other.initial_states )
		final_states = set( (left, s) for s in self.final_states )
		final_states.update( (right, s) for s in other.final_states )
		return super().union(other, labels=(left, right), initial_states=initial_states, final_states=final_states)

	def is_deterministic(self):
		return len(self.initial_states) == 1 and super().is_deterministic()

	@abc.abstractmethod
	def run(self, inputs):
		pass

