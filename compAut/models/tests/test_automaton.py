from compAut.metas import allExcept
from compAut.models import Automaton
from compAut.tests.utils import sorted_h
import itertools

def check_aut(A, accepting, rejecting):
	for u in accepting:
		assert u in A
	for u in rejecting:
		assert u not in A

class TestAutomaton:
	def setup_method(self):
		"""
		A accepts languages (.*)ab
		B accepts languages [^d]*
		C accepts languages (ab)*
		"""
		transitions_tuple = tuple(((0, allExcept(), 0), (0, 'a', 0), (0, 'a', 1), (1, 'b', 2)))
		A = Automaton.from_transitions(transitions_tuple, initial_states=[0], final_states=[2])
		A_accepting = ["ab", "aab", "xéab", (0, 1, 'a', 'b'), 'Ĉ'*100+'ab']
		A_rejecting = ['', 'a', 'aabc', (0, 1, 'a', 'b', 'a'), 'Ĉ'*100+'ab'+'Ĉ']
		B = Automaton.from_transitions(((0, allExcept(["d"]), 0),), initial_states=[0], final_states=[0])
		B_accepting = ["a", "b", "c", "aabbbaaac"]
		B_rejecting = [u+"d"+v for u in B_accepting for v in B_accepting]
		B_rejecting = [u+"d"+v for u in B_rejecting for v in B_rejecting]
		C = Automaton.from_transitions(((0, 'a', 1), (1, 'b', 0)), initial_states=[0], final_states=[0])
		C_accepting = ["", "ab", "ab"*100]
		C_rejecting = ["b", "ba", "ab"*100+"b", "ab"*10+"c"+"ab"*100]
		D = Automaton.from_transitions(((0, allExcept(), 1), (1, allExcept("a"), 2), (1, "a", 0), (2, allExcept(), 2)), initial_states=[0], final_states=[0]) # Aut of "(.a)*"
		D_accepting = ["bababa", "ôabaôa", "☂a☂a☂a"]
		D_rejecting = ["babab", "ôabôa", "a☂a☂a"]
		self.exemples = ((A, A_accepting, A_rejecting), (B, B_accepting, B_rejecting), (C, C_accepting, C_rejecting), (D, D_accepting, D_rejecting))

	def test_complement(self):
		for A, acc, rej in self.exemples:
			Complement = A.complement()
			check_aut(Complement, rej, acc)

	def test_complete(self):
		for A, acc, rej in self.exemples:
			check_aut(A.complete(), acc, rej) #should not change anything

	def test_concatenate(self):
		for A, acc, _ in self.exemples:
			for B, acc2, _ in self.exemples:
				check_aut(A.concatenate(B), [tuple(u)+tuple(v) for u in acc for v in acc2], [])

	def test_difference(self):
		A = self.exemples[0][0]
		B = self.exemples[1][0]
		Diff = A.difference(B) # Ends with "ab" and should contains a d
		acc = ["dab", "dXab"]
		rej = ["ab", "xxxxab", "dsqdazezezae"]
		check_aut(Diff, acc, rej)

	def test_intersection(self):
		A = self.exemples[0][0]
		B = self.exemples[1][0]
		I = A.intersection(B) # Ends with "ab" and should not contains a d
		acc = ["ab", "Xab", "XXXXab"]
		rej = ["dab", "xxdxxab", "zezaea"]
		check_aut(I, acc, rej)

	def is_determinsitic(self):
		assert not self.exemples[0][0].is_deterministic()
		assert not self.exemples[1][0].is_deterministic()

	def test_isaccepted(self):
		for A, acc, rej in self.exemples:
			for u in acc:
				assert A.is_accepted(u)
			for u in rej:
				assert not A.is_accepted(u)
	def test_kleene(self):
		for A, acc, _ in self.exemples:
			K = A.kleene_star()
			assert "" in K
			k_acc = list(acc)
			for _ in range(3):
				k_acc += [tuple(u) + tuple(v) for u in k_acc for v in acc]
			check_aut(K, k_acc, [])

	def test_minimize(self):
		for A, acc, rej in self.exemples:
			M = A.minimize()
			check_aut(M, acc, rej) #should not change anything
			assert M.is_deterministic()

	def test_reverse(self):
		for A, acc, rej in self.exemples:
			acc = list(map(reversed, acc))
			rej = list(map(reversed, rej))
			check_aut(A.reverse(), acc, rej)

	def test_trim(self):
		for A, acc, rej in self.exemples:
			check_aut(A.complete(), acc, rej) #should not change anything

	def test_union(self):
		for A, acc, _ in self.exemples:
			for B, acc2, _ in self.exemples:
				u_acc = acc+acc2
				check_aut(A.union(B), u_acc, [])
	def test_expanded_chars(self):
		for A, acc, rej in self.exemples:
			if any(map(lambda e: type(e) is not str, itertools.chain(acc, rej))):
				continue
			check_aut(A.expanded_utf8_chars(), map(str.encode, acc), map(str.encode, rej))
