from compAut.models.finiteautomata import Automaton, word_to_automaton, set_to_automaton, allExcept, allIn, metaLetter, words_to_automaton
import lark
from string import ascii_letters as letters
from compAut.encodings import Unicode, ASCII


class ClassicalTransformer(lark.InlineTransformer):
	def values(self, child):
		if "value" in dir(child):
			child = child.value
		return set_to_automaton(child)
	def all(self):
		return Automaton.from_transitions(((0, allExcept(), 1),), initial_states=[0], final_states=[1])
	def range(self, Left, Right):
		return allIn(chr(i) for i in range(ord(Left.value), ord(Right.value)+1))
	def preset(self, *children):
		return allIn().union(*children)
	def set(self, *children):
		"""
		children should al be set like.
		"""
		negate = False
		if "data" in dir(children[0]) and children[0].data == "compset":
			negate = True
			children = children[1:]
		S = set()
		for child in children:
			if isinstance(child, set) or isinstance(child, metaLetter):
				S.update(child)
			else:
				S.add(child.value)
		if negate:
			Meta = allExcept(S)
		else:
			Meta = allIn(S)
		return set_to_automaton(Meta)
	def union(self, left, right):
		return left.union(right)
	def difference(self, left, right):
		return left.difference(right)
	def kleene(self, left):
		return left.kleene_star()
	def optional(self, left):
		return left.union(word_to_automaton(""))
	def values_intern(self, child):
		return child
	def atleastone(self, left):
		return left.concatenate(left.kleene_star())
	def complement(self, left):
		return left.complement()
	def expr(self, el):
		return el
	def simpleexpr(self, el):
		return el
	def count(self, left, atleast, *args):
		A = left
		atleast = int(atleast)
		if not args:
			return A.iterate(atleast)
		else:
			if len(args)>1 and args[1].type == "INT":
				atmost = int(args[1])
				return A.iterate(atmost, range(atleast, atmost+1))
			if args[0].type == "COMMA":
				return A.iterate(atleast).concatenate(left.kleene_star())
		raise ValueError(f"Unknown {args} type")
	def escaped(self, Escaped):
		if Escaped.value == 'w':
			return allIn(letters)
		if Escaped.value == "s":
			return allIn(" \t\n\r\f\v")
		if Escaped.value == "d":
			return allIn("0123456789")
		else:
			return allIn([Escaped.value])
	def expr(self, child, *children):
		A = child
		for el in children:
			A = A.concatenate(el)
		return A.minimize()
	def simple_expr(self, child):
		return child
	def start(self, child):
		return child

cached_automata = {}
def encoded_cached_automata(descr):
	if descr not in cached_automata:
		if isinstance(descr, bytes):
			cached_automata[descr] = word_to_automaton(descr)
		else:
			cached_automata[descr] = words_to_automaton(*descr)
	return cached_automata[descr]

class PCRETransformer(lark.InlineTransformer):
	char_mapping = {
		"d": "decimal_digits",
		"h": "horizontal_whitespace",
		"v": "vertical_whitespace",
		"s": "vertical_whitespace",
		"w": "word_char"
	}
	def __init__(self, encoding=ASCII()):
		self.encoding = encoding
	def hex_code(self, value):
		return bytes.fromhex(value.value)
	def octal_code(self, value):
		s = hex(int(value.value, 8))[2:]
		if len(s) % 2 == 1:
			s = '0' + s
		return bytes.fromhex(s)
	def non_print_char(self, escped_char, value):
		if hasattr(value, "data"):
			return getattr(self.encoding, value.data)
		return value
	def char_types(self, child, value):
		flag = value.value
		if flag in self.char_mapping:
			prop = self.char_mapping[flag]
			return allIn(getattr(self.encoding, prop))
		elif flag.lower() in self.char_mapping:
			prop = self.char_mapping[flag.lower()]
			return allExcept(getattr(self.encoding, prop))
		raise ValueError(f"Unknown character \\{flag}")

	def range(self, child, *children):
		return child
	def posix_set(self, child, *children):
		return child
	def posix_set_descr(self, child, *children):
		return child
	def positive_class(self, child):
		if not isinstance(child, metaLetter):
			return allIn(child)
		return child
	def negative_class(self, neg, value, *values):
		return value.union(*values).complement()

	def square_base_value(self, value):
		return value

	def square_literal_value(self, value):
		if not isinstance(value, metaLetter):
			return allIn(value)
		return value

	def range_literal_value(self, value):
		if hasattr(value, "value"):
			return self.encoding(value.value)
		return value

	def range(self, low, range_symb, high):
		return allIn(range(low, high))

	def char_class(self, left_bracket, value, *values):
		values = values[:-1]
		return Automaton.from_transitions([(0, value.union(*values), 1)], initial_states=[0], final_states=[1])
	def base_value(self, value):
		return self.encoding(value.value)
	def literal_value(self, value):
		if isinstance(value, metaLetter):
			return Automaton.from_transitions([(0, value, 1)], initial_states=[0], final_states=[1])
		return Automaton.from_transitions([(0, allIn(value), 1)], initial_states=[0], final_states=[1])

	def all_char(self, ALL_CHAR):
		return Automaton.from_transitions([(0, allExcept(),1)], initial_states=[0], final_states=[1])

	def base_pattern(self, child, *children):
		return child
	def branch_pattern(self, child, *children):
		return child.union(*filter(lambda e:isinstance(e, Automaton), children))
	def subpattern(self, start, sub, end):
		return sub
	def qualified_pattern(self, child, *children):
		return child
	def name_of_pattern(self, child, *children):
		return child
	def named_pattern(self, child, *children):
		return child
	def captured_pattern(self, child, *children):
		return child
	def repetition_pattern_base(self, child):
		return child
	def repetition_pattern(self, pattern, quantifier):
		if quantifier.type == "KLEENE_STAR":
			return pattern.kleene_star()
		elif quantifier.type == "KLEENE_PLUS":
			return pattern.kleene_star().concatenate(pattern)
		elif quantifier.type == "ZO_QUANTIFIER":
			return pattern.union(word_to_automaton(()))
		raise NotImplementedError(f"Quantifier {quantifier.type} is not yet implemented")
	def quantifier(self, child):
		return child
	def non_greedy_quantifier(self, child, *children):
		return child
	def pattern(self, *children):
		A = word_to_automaton(())
		for child in children:
			A = A.concatenate(child)
		return A
	def base_assertion(self, child, *children):
		return child
	def positive_lookahead(self, child, *children):
		return child
	def negative_lookahead(self, child, *children):
		return child
	def lookahead(self, child, *children):
		return child
	def positive_lookbehind(self, child, *children):
		return child
	def negative_lookbehind(self, child, *children):
		return child
	def lookbehind(self, child, *children):
		return child
	def assertion(self, child, *children):
		return child
	def start(self, child):
		return child.minimize()

