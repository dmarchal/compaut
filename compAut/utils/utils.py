from PIL import Image               # to load images
from collections.abc import Mapping, MutableSet
from .pretty_code import code_repr
def shorten(s, size=17):
	if len(s) < size:
		return s
	return s[:size//2] + "…" + s[-size//2:]

def show_img(path):
	img = Image.open(path)
	return img

import copy

def recursive_shallow_dictcopy(rec_dict):
	"""
	Copy recursively dict (of dict)* without copying
	objects.
	"""
	d = copy.copy(rec_dict)
	for k in d:
		if isinstance(d[k], dict):
			d[k] = recursive_shallow_dictcopy(d[k])
	return d

# Extracted from networkdisk.
# Should it be somewhere else?

class ProtectedDict(Mapping):
	def __init__(self):
		self.map = {}
	def __getitem__(self, k):
		return self.map[k]
	def __len__(self):
		return len(self.map)
	def __iter__(self):
		return iter(self.map)
	def __repr__(self):
		return f"{repr(self.map)}ₚ"

class BinRel(Mapping, MutableSet):
	"""
	A binary relation between hashable Python elements.
	See the Wikipedia page (https://en.wikipedia.org/wiki/Binary_relation)

	Allow to maintains inverse of dictionary simply.
	Semantics of the object itself is a containers of couple.

	D.left[x] provides all the y such that (x, y) is in D
	D.right[x] provides all the y such that (y, x) is in D.

	D[x] provides the couple (D.left[x], D.right[x]).

	D.left and D.right are *protected dict*, that is Dict that
	are not meant to be updated directly and raise an error
	if not used appropriately.

	Example of usage:
	>>> D = BinRel()
	>>> D.add((0, 1)) # We add the pair (0, 1) to the relation.
	>>> D.update([(1, 2), (3, 4)]) # we can add multiple pairs.
	>>> D[0]
	({1}, ())
	>>> D[1]
	({2}, {0})
	>>> D.left
	{0: {1}, 1: {2}, 3: {4}}ₚ
	>>> D.right
	{1: {0}, 2: {1}, 4: {3}}ₚ

	For all situation where dict and their inverse are of interest,
	BinRel can be useful as they maintain the hashmap in both direction.

	"""
	def __init__(self, *E):
		self.left = ProtectedDict()
		self.right = ProtectedDict()
		self.update(*E)
	def __contains__(self, leftNright):
		left, right = leftNright
		return right in self.left.get(left, ())
	def __len__(self):
		return sum(map(len, self.left.values()))
	def __iter__(self):
		for k, v in self.left.items():
			for w in v:
				yield (k, w)
	def __getitem__(self, key):
		left = self.left.get(key, ())
		right = self.right.get(key, ())
		if not left and not right:
			raise KeyError(key)
		return left, right
	def __delitem__(self, key):
		target = (self.left, self.right)
		for i in range(2):
			res = target[i].map.pop(key, ())
			for r in res:
				s = target[i-1][r]
				s.remove(key)
				if not s:
					target[i-1].map.pop(r)
	def add(self, leftNright):
		left, right = leftNright
		self.left.map.setdefault(left, set())
		self.left.map[left].add(right)
		self.right.map.setdefault(right, set())
		self.right.map[right].add(left)
	def discard(self, leftNright):
		left, right = leftNright
		if left not in self.left or right not in self.right:
			return
		self.left.map[left].discard(right)
		if not self.left.map[left]:
			del self.left.map[left]
		self.right.map[right].discard(left)
		if not self.right.map[right]:
			del self.right.map[right]
	def update(self, *E):
		for it in E:
			for k, v in it:
				self.add((k, v))
	def __repr__(self):
		return f"{{{', '.join('↔'.join(map(str, e)) for e in self)}}}"
	def union(self, other):
		#TODO: optimize?
		return BinRel(self, other)
	def keys(self):
		yield from self.left
		yield from filter(lambda k: k not in self.left, self.right)

