import os
from setuptools import setup, find_packages
import compAut as ca
def read(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).read()
packages = find_packages()
print(packages)
setup(
	name = "CompAut",
	version = f"{ca.__version__}",
	keywords = "Regexp, Automaton, Compilation",
	url = "https://gitlab.inria.fr/cpaperma/compaut",
	author= "Charles Paperman",
	author_email = "charles.paperman@univ-lille.fr",
	install_requires = ["lark-parser", "networkx", "matplotlib", "pillow" ],
	package_data = {"": ["*.lark", "*.c", "*.json"]},
	licence = "MIT",
	packages=packages,
	include_package_data=True,
	long_description=read('README.md'),
	classifiers=[
		"Development Status :: 0 - Alpha",
		"Topic :: Utilities",
		"License :: OSI Approved :: MIT License",
		"Operating System :: Unix",
	],
)
